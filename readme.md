# Slate Terminal
A simple terminal interface to embed on a webpage.  It handles the basics of the interface while you control what it does.

## Features
- simple to implement with basic input and output text functions
- style your own way
- command history with up/down
- optional JavaScript eval mode
- a few extra features like text padding

<img src="https://gitlab.com/lucidlylogicole/slate/raw/demos/slate_screenshot.png">

## Requirements
- A modern HTML5 browser
- No additional modules like jquery are needed

## License
- [MIT](license.txt)

## Installation
1. Download the [slate-terminal.js](https://gitlab.com/lucidlylogicole/slate/raw/master/slate-terminal.js) file
2. Reference it on your html page

## Demos
- **[Demo - Full Screen](https://lucidlylogicole.gitlab.io/slate/demo_fullscreen.html)**
    - A full screen demo with the default commands included (default-commands.js)
    - JavaScript eval is enabled (will process JavaScript commands)
- **[Demo - Small](https://lucidlylogicole.gitlab.io/slate/demo_small.html)**
    - A small screen demo with a fixed command input at the bottom
    - JavaScript eval is enabled (will process JavaScript commands)

## Quickstart
1. Include the slate-terminal.js file

        <script src="slate-terminal.js"></script>

2. Add pre and textarea elements

        <div class="terminal-box">
            <pre class="terminal-output" id="termoutput"></pre>
            <div class="terminal-input">
                <span id="prompt">&rsaquo;</span>
                <textarea id="terminput" rows="1"></textarea>
            </div>
        </div>

3. Initialize the terminal class by passing the ids of the input (textarea) and output (pre) elements

        <script>
            term = new Terminal('terminput','termoutput')
            
            // Add a custom command to the terminal
            term.setCommand('hi', function() {return 'hello!'})
            
        </script>

4. Style the terminal however you want, but here is a basic style:

        <style>
            .terminal-box {
                background:rgba(30,30,30,0.9);
            }
            
            .terminal-output {
                color:white;
                height:300px;
                margin:0;
                padding:10px;
                white-space: pre-wrap; 
            }
            
            .terminal-input {
                border-top:1px solid rgba(80,80,80,0.6);
                display:flex;
            }
            
            .terminal-input > span {
                color:white;
            }
            
            .terminal-input textarea {
                resize:none;
                border:0;
                outline:0;
                flex:1;
                color:white;
                background:transparent;
                padding:4px;
            }
        </style>

----

# Documentation

## Terminal('input_element', 'output_element')
The main terminal object
- **input_element** - the id of the input element (textarea)
- **output_element** - the id of the output element (pre)

        let term = new Terminal('input_element', 'output_element')

## Settings
The settings can be set after creating a new terminal object

        let term = new Terminal('input_element', 'output_element')
        term.echo_commands = 0

- **Terminal.echo_commands=1** - write input commands on screen (default is 1/true)
- **Terminal.char_width=80** - width of the screen in characters (this may be used by some of the padding calculations) (default is 80)
- **Terminal.eval_enabled=0** - enable the terminal to parse the command as JavaScript if no other specified commands are found (default is 0/false)
- **Terminal.eval_mode='global'** - use global or local scope for the JavaScript eval (default is 'global')
- **Terminal.track_history=0** - keep track of input command history (default is 0/false)

## Commands
The Slate Terminal works by processing a list of commands you specified.  Those commands are stored in Terminal.commandD

- the command function can return a string that will then automatically be output on the terminal
- the command function can also handle an arguments which is any text the user types (after the space) after command text
    - the rest of the text after command is returned as a single string.  It is up to you to process that for your command.
- commands can be tied to multiple apps or to the 'main' app

## Add / Set a Command
**term.setCommand('mycommand', myfunction)**

Using the setCommand function instead of the dictionary directly will also bind the Terminal object to the *this* scope of the function

When a user types *mycommand* in the terminal, it will execute *myfunction* (after they press return).  The Terminal object will be binded to *this* scope of the function.

__Example 1:__

        Terminal.setCommand('hi', function(){return 'hello'}
        
- When a users types *hi* then *hello* will be output on the screen

         _______________________
        | > hi                  |
        | hello                 |
        |                       |
        |_______________________|
        |> hi                   |  
        |_______________________|


__Example 2:__

        Terminal.setCommand('hi', function(txt){return 'hello '+txt}

- Any additional text after the space after the command will be returned to the function
- When a user types *hi Joe* then the string *Joe* will be passed to the function and the output will be *hello Joe*

         _______________________
        | > hi Joe              |
        | hello Joe             |
        |                       |
        |_______________________|
        |> hi Joe               |  
        |_______________________|


## Terminal Functions
Some functions of the Terminal object that you may want to use

- **.clearInput()** - clears the input box
- **.setInputText(txt)** - sets the input text but does not execute
- **.inputCommand(cmd_txt)** - executes the command
- **.output(txt, auto_scroll)** - adds text to the output screen (pre)
    - *auto_scroll* - terminal automatically scrolls the output element.  set this to 0 to avoid scrolling
- **.clearOutput()** - clears the output screen
- **.centerText(txt)** - returns text with left padding to center text on the screen
    - this function utilizes Terminal.char_width - which you may need to change
- **.lpadText(txt, width,space)** - adds *width* number of spaces before the text
- **.rpadText(txt, width, space)** - adds spaces after the text to make sure the text length is equal to *width*
- **.rightAlignText(txt, width, space)** - adds spaces before the text to make sure the text length is equal to *width*
- **splitLines(txt)** - split text into an array of lines
- **.setCommand('mycommand', myfunction)** - sets the command text *mycommand* to the function *myfunction*
- **.setCommands(commandDict)** - sets all commands from the *commandDict* dictionary to the terminal
- **.getInput()** - pause execution of function and get the user input (your function needs to be async and precede this with await)

## Input Key Functions
The Slate Terminal input handles the following key functions:
- **Enter** - executes the command by calling *Terminal.inputCommand*
- **Shift+Enter** - adds a new line on the input
- **Up Arrow** - goes to the previous entered command (if command history is enabled)
- **Down Arrow** - scrolls down one through the command history (if command history is enabled)
- **Page Up** - scrolls the output up by 80% of the height
- **Page Down** - scrolls the output down by 80% of the height

## Apps
The Slate Terminal has one app named *main* that is the default app.  Any commands  are tied to the main app by default.  For more complicated setups, you can have apps that have their own separate command sets.

- app commands are only evaluated for the current app
- to set a command to a specific app, there is an optional app argument
    - **.setCommand('mycommand', myfunction, appname)**
    - **.setCommands(commandDict, appname)**
- if a command is equal to an app name, the terminal will set that as the current app
    - you can disable this by setting
        - term.app_cmd_opens_app = 0
- **.setCurrentApp(app_name)** - to set the current app use
- **.addApp(app)** - add an app with an app object

        let myapp = new App('myapp')
        term.addApp(myapp)


- **.removeApp(app_name)** - remove an app

### App Object
Slate Terminal will automatically create new apps with the setCommand function.  For more control you can create your own app object

- **App(app_name, commandDict)** - the main app object
    - *app_name* (str) - the name of the app
    - *commandDict* - (optional) a dictionary with a list of commands and associated functions
    - **.commands** - a dictionary containing the list of commands
    - **.onload()** - a function you can override which will be run every time the app is loaded
    - **inputCommandFunction(cmd_txt)** - optional function that can be provided that overrides the default input command parsing function (for that app)
    

        // Create App (with print function)
        myapp = new App('myapp', {'print':function(txt) {return txt}})
        myapp.onload = function(){'do something when loading this app'}
        myapp.commands['hi'] = function(){return 'hello'}  // add a command called hi

## Events
- **outputChanged** - event is triggered when the output function is called
    - to use this event to scroll the window:
        
            window.addEventListener('outputChanged', function (e) {
                window.scrollTo(0,document.body.scrollHeight);
            })

- **currentAppChanged** - Event triggered when the current app is changed.
    - to listen to this event:
    
            window.addEventListener('currentAppChanged', function (e) {
                txt = ''
                if (e.detail != 'main') {txt = e.detail}
                document.getElementById('prompt').innerHTML = txt +' '+ '&rsaquo;'
            })

## Wait for User Input
The Slate Terminal has a way to block execution, await a response, and get that response through the **getInput** function.  The getInput is a promise, so you have to use async/await to block in your function.  The following example has a command *dogyears* that will ask for your age, wait and then calculate and return a result.

        term.setCommand('dogyears', async function(){
            term.output('Enter your age:')
            v = await term.getInput() // Wait for user input
            term.output('\nYour dog age is ' + (parseInt(v)/7).toFixed(1) + ' years')
        })

## Default Commands
The terminal does not have any default commands set up so that you have full control. For some useful default commands, you load a default command set.


To load the default commands:

        addDefaultCommands(term)

The following commands are included:

- **clear** - clears the output screen
- **exit** - will exit the current app (set the current app to main)
- **?** - show a simple help screen
- **? commands** - list out the commands of the current app
