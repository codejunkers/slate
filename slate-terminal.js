let version = '1.5.1'

class Terminal {
    constructor(inputElement,outputElement) {
        this.elm_in = document.getElementById(inputElement)
        this.elm_out = document.getElementById(outputElement)
        
        this.elm_in.onkeydown = this.terminalKeyDown.bind(this)
        
        this.elm_in.focus()
        // this.commandD = {main:{}}       // Stores the list of commands
        this.current_app = 'main'        // Stores the current app
        // this.input_command_functionD = {} //{main:this.mainInputCommand} // Stores input command function override
        
        // Setup Main App
        let MainApp = new App('main')
        this.apps = {
            main:MainApp
        }
        
        // Settings
        this.echo_commands = 1      // write input commands on screen
        this.char_width = 80        // width of screen in characters
        this.app_cmd_opens_app = 1  // Open the app (set current app) if the app is input as a command
        
        // Enable JavaScript eval (runs if commands not found)
        this.eval_enabled = 0
        this.eval_mode = 'global'  // global or local scope for eval
        
        this.wait_for_input = 0     // Check when using get_input
        
        // Keep track of command history
        this.track_history = 0
        this.command_history = []
        this.history_position = 0
    }

    //---Commands
    setCommand(cmd,fnc,app) {
        if (app === undefined) {app = 'main'}
        else if (!(app in this.apps)) {
            let new_app = new App(app,{cmd:fnc})
            this.addApp(new_app)
        }
        this.apps[app].commands[cmd] = fnc
    }
    
    setCommands(cmdDict,app) {
        if (app === undefined) {app = 'main'}
        else if (!(app in this.apps)) {
            this.addApp(app)
        }
        for (let cmd in cmdDict) {
            this.setCommand(cmd,cmdDict[cmd],app)
        }
    }

    //---Apps
    setCurrentApp(app_name) {
        this.current_app = app_name
        this.currentAppChanged(app_name) 
        this.apps[app_name].onload()
    }
    
    currentAppChanged(app_name) {
        let evt = new CustomEvent('currentAppChanged', {detail: app_name});
        window.dispatchEvent(evt);
    }
    
    // addApp(app_name, inputFunction) {
        // this.apps[app_name] = {
        //     commands:{},
            
        //     // settings:{},
        // }
        
        // if (inputFunction !== undefined) {
        //     this.apps[app_name].input_command_function = inputFunction.bind(this)
        // }
        // return this.apps[app_name]
    addApp(app) {
        this.apps[app.name] = app
    }
    
    removeApp(app_name) {
        if (! app_name == 'main') {
            
            if (this.current_app == app_name) {
                this.current_app = 'main'
            }
            
            // Remove app if not in main
            delete this.apps[app_name]
        }
    }

    //---Input Commands
    terminalKeyDown(event) {
        // console.log(event.keyCode)
        let handled = 0
        if (event.shiftKey === true && event.keyCode == 13) {
            // Shift Enter
            this.elm_in.rows=this.elm_in.rows+1
        } else if (event.keyCode == 13) {
            // Enter
            handled= 1
            if (this.wait_for_input) {
                this.wait_for_input = 0
                // Clear wait for input and allow that to handle input
            } else {
                this.inputCommand(this.elm_in.value)
                this.clearInput()
            }
        } else if (event.keyCode == 33) {
            // Page Up
            this.elm_out.scrollTop -= this.elm_out.offsetHeight*0.8
            handled = 1
        } else if (event.keyCode == 34) {
            // Page Down
            this.elm_out.scrollTop += this.elm_out.offsetHeight*0.8
            handled = 1
        } else if (this.track_history) {
            if (event.keyCode == 38) {
                // Up Arrow
                this.inputHistory(-1)
                handled = 1
            } else if (event.keyCode == 40) {
                // Down Arrow
                this.inputHistory(1)
                handled = 1
            }
        }

        // Prevent Key press
        if (handled) {event.preventDefault()}
    }
    
    clearInput() {
        this.elm_in.value = ''
        this.elm_in.rows = 1
    }
    
    setInputText(txt) {
        this.elm_in.value = txt
        this.elm_in.selectionStart = this.elm_in.selectionEnd = this.elm_in.value.length;
    }

    inputCommand(cmd_txt) {
        if (this.apps[this.current_app].inputCommandFunction !== undefined) {
            this.apps[this.current_app].inputCommandFunction(cmd_txt)
        } else {
            this.mainInputCommand(cmd_txt) 
        }
    }
    
    getInput() {
        this.wait_for_input = 1
        let term = this
        return new Promise((resolve,reject)=>{
            function check() {
                if (term.wait_for_input) {
                    setTimeout(check,100)
                } else {
                    term.wait_for_input = 0
                    let v = term.elm_in.value
                    term.clearInput()
                    resolve(v)
                }
            }
            setTimeout(check,100)
        })
    }
    
    mainInputCommand(cmd_txt) {
        if (this.echo_commands) {
            // this.elm_out.innerHTML += '<div class="cmd">&rsaquo; '+cmd_txt+"</div>"
            this.output('<div class="cmd">&rsaquo; '+cmd_txt+"</div>")
        } else {
            this.elm_out.innerHTML += '\n'
        }
        
        // Store History of Command
        if (this.track_history) {
            this.storeHistory(cmd_txt)
        }
        
        // Split for command
        let cmd, cmd_args
        let cmd_match = cmd_txt.match(/^(\S+)\s(.*)/)
        if (cmd_match !== null) {
            let cmd_split = cmd_match.slice(1)
            cmd = cmd_split[0]
            cmd_args = cmd_split[1]
        }
        
        // Check for full text command in Current App
        if (cmd_txt in this.apps[this.current_app].commands) {
            let out = this.apps[this.current_app].commands[cmd_txt]()
            if (out !== undefined && !(out instanceof Promise)) {
                this.output(out)
            }
        // Check for first part of command in Current app
        } else if (cmd in this.apps[this.current_app].commands) {
            let out = this.apps[this.current_app].commands[cmd](cmd_args)
            if (out !== undefined) {
                this.output(out)
            }
        // If apps enabled, check if full command in app
        } else if (this.app_cmd_opens_app && (cmd_txt in this.apps)) {
            // Check if command is the name of an app (if so open)
            this.setCurrentApp(cmd_txt)
        } else if (this.current_app != 'main') {
            // Check if full command is in main application
            if (cmd_txt in this.apps.main.commands) {
                let out = this.apps.main.commands[cmd_txt]()
                if (out !== undefined) {
                    this.output(out)
                }
            // Check if start of command in current application
            } else if (cmd in this.apps[this.current_app].commands) {
                let out = this.apps[this.current_app].commands[cmd](cmd_args)
                if (out !== undefined) {
                    this.output(out)
                }
            }
        } else if (this.eval_enabled){
            // Run JavaScript command if enabled
            try {
                if (this.eval_mode == 'global') {
                    this.output(window.eval(cmd_txt))    // Using Global scope
                } else if (this.eval_mode == 'local') {
                    this.output(eval(cmd_txt))       // Using local scope
                }
            } catch(e) {
                this.output('<i>undefined or error</i>')
                // console.log(e)
            }
        }
    }

    storeHistory(cmd_txt) {
        // Store History
        let ok = 1
        if (this.command_history.length >0) {
            // Ignore history if command is the same
            if (this.command_history[this.command_history.length -1] == cmd_txt) {
                ok = 0
            }
        }
        if (ok) {
            this.command_history.push(cmd_txt)
        }
        
        // Add History Count
        this.history_position = this.command_history.length
    }

    inputHistory(inc) {
        // Get input command history (1 for above, -1 for previous)
        if (this.command_history.length > 0) {
            
            this.history_position += inc
            let end_flg = 0
            
            if (this.history_position >= this.command_history.length) {
                this.history_position -= 1
                this.setInputText('')
                end_flg = 2
            } else if (this.history_position < 0) {
                this.history_position = 0
            }
            
            if (!end_flg) {
                this.setInputText(this.command_history[this.history_position])
            }
        }
    }

    //---Output
    output(out_txt,auto_scroll) {
        this.elm_out.innerHTML += out_txt
        
        // Autoscroll output element (default is true)
        if (auto_scroll === undefined) {auto_scroll = 1}
        if (auto_scroll) {
            this.elm_out.scrollTo(0,this.elm_out.scrollHeight);
        }
        
        this.outputChanged() // Output Event
    }

    outputChanged() {
        let evt = new CustomEvent('outputChanged', {});
        window.dispatchEvent(evt);
    }

    clearOutput() {
        // this.elm_out.innerHTML = ''
        this.output('')
    }

    // Print current app command list
    printCommandList() {
        this.output('HELP:\n  Commands for the current app <i><b>'+this.current_app+'</b></i>:')
        for (let ky in this.apps[this.current_app].commands) {
            this.output('\n    '+ky)
        }
        
        // Show Apps if main
        if (this.current_app == 'main') {
            this.output('\n  Available apps:')
            for (let ky in this.apps) {
                if (ky != 'main') {
                    this.output('\n    '+ky)
                }
            }
        
        }
    }

    //---Text Utils
    centerText(txt) {
        // Return left space padded text that will center on screen
        let centered_txt = ''
        for (let line of this.splitLines(txt)) {
            let pad = Math.floor((this.char_width - line.length)/2)
            if (centered_txt.length > 0) {centered_txt += '\n'}
            centered_txt += this.lpadText(line,pad)
        }
        
        return centered_txt
    }

    lpadText(txt, width, space) {
        // Add padding to the left of text
        space = space || ' '
        txt = txt + ''
        return space.repeat(width)+txt
    }

    rpadText(txt, width, space) {
        // Add padding to the right of the text
        space = space || ' '
        txt = txt + ''
        return txt.length >= width ? txt : txt + space.repeat(width - txt.length)
    }

    rightAlignText(txt, width, space) {
        // Pad text so it is right aligned
        space = space || ' '
        txt = txt + ''
        return txt.length >= width ? txt : space.repeat(width - txt.length) + txt
    }

    splitLines(txt) {
        // Split text into an array of lines
        return txt.match(/[^\r\n]+/g)
    }
   
}

//---App
class App {
    constructor(name, commandD) {
        this.name = name
        this.settings = {}
        this.commands = {}
        
        if (commandD != undefined) {
            this.commands = commandD
        }
        
    }
    
    onload(){} // onload function
    // Optional Override of input command function
    // inputCommandFunction() {
        
    // }
    
    appChanged() {
        // When 
    }
}


//---Default Commands
// Some useful commands to add to main
function addDefaultCommands(term, app_name) {
    DefaultCommands = {
        'clear':term.clearOutput.bind(term),            // Clear the screen
        'exit':function(){term.setCurrentApp('main')},  // exit any apps (goto main)
        '? commands':term.printCommandList.bind(term),                   // Help - view the list of commands for current app
        '?':function(){
            term.output('HELP:\n  To see the list of commands for the current app type:\n     <i>? commands</i>')
        },
    }
    
    if (!app_name) {
        app_name = 'main'
    }
    
    term.setCommands(DefaultCommands, app_name) // add default commands to main
}
// To add these:
// addDefaultCommands(terminal_name)